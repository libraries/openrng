/*
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0 WITH LLVM-exception
 */

#include <utils/cpu_features.hpp>

namespace openrng {

const bool SupportedFeatures::hasTrng = true;

} // namespace openrng

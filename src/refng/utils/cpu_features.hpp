/*
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0 WITH LLVM-exception
 */

#pragma once

namespace refng {

/**
 * Supported CPU Features.
 *
 * refng implementations should always be generic.
 */
enum class CPUFeatures { generic };

} // namespace refng

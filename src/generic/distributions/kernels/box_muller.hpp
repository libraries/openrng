/*
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0 WITH LLVM-exception
 */

#pragma once

#include <generator/generator.hpp>
#include <openrng.h>
#include <utils/concepts.hpp>
#include <utils/cpu_features.hpp>

#include <cassert>
#include <cmath>
#include <numbers>

namespace openrng::distribution::gaussian {

template <CPUFeatures> struct BoxMuller {};

/**
 * Creates a Gaussian sequence using the Box Muller 1 method.
 *
 * The Box Muller 1 method applies the following transformation to two uniformly
 * distributed variables to generate a Gaussian distributed number.
 *
 *   x = sqrt(-2 * log(u1)) * sin(2 * std::numbers::pi * u2);
 *
 * This can be further transformed to a Gaussian distribution with standard
 * deviation, and mean a, using
 *
 *   y = s * x + a;
 *
 * Once this is completed, y is stored in r.
 *
 * Assumes n > 0.
 * Assumes generator is a pointer to a valid generator.
 * Assumes r is of length n.
 */
template <> struct BoxMuller<CPUFeatures::generic> {
  template <openrng::FloatingPoint T>
  static int fill(Generator *generator, const int64_t n, T r[], const T a,
                  const T sigma) {
    assert(n > 0);
    assert(generator != nullptr);

    // uniforms[] is an array that stores all the u1 and u2s given by the chosen
    // generator that are used in the Box-Muller formula
    T uniforms[2];

    for (int64_t i = 0; i < n; i++) {
      generator->fill(2, uniforms);
      const auto [u1, u2] = uniforms;

      // Box-muller uses x an y for generating the numbers of the distribution
      // as follows: x = sqrt(-2 * log(u1)) * sin(2 * std::numbers::pi * u2),
      // and then by using the parameters a and sigma to generate the random
      // number y = sigma * x + a
      const auto x = sqrt(-2 * log(u1)) * sin(2 * std::numbers::pi * u2);
      const auto y = sigma * x + a;

      // r[] is an array given as a parameter that stores the random numbers
      // generated by this distribution
      r[i] = y;
    }

    return VSL_ERROR_OK;
  }
};
} // namespace openrng::distribution::gaussian

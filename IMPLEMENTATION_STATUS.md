# Implementation Status

This file lists the current implementation status of each generator and
distribution defined in the API.

### Implemented

#### Generators

The following generators are implemented in this version of OpenRNG:

 - MCG31
 - MCG59
 - MRG32K3A
 - MT19937
 - NONDETERM
 - PHILOX4X32X10
 - R250
 - SFMT19937
 - SOBOL

#### Distributions

The following distributions are implemented in this version of OpenRNG:

 - Bernoulli
 - Binomial
 - Cauchy
 - Exponential
 - Gaussian
 - Geometric
 - Gumbel
 - Laplace
 - Lognormal
 - Poisson
 - Rayleigh
 - Uniform
 - Uniform
 - UniformBits
 - UniformBits32
 - UniformBits64
 - Weibull


### Not Implemented

#### Generators

The following generators are _not_ implemented in this version of OpenRNG:

 - WH
 - NIEDERR
 - MT2203
 - IABSTRACT
 - DABSTRACT
 - SABSTRACT
 - ARS5

#### Distributions

The following distributions are _not_ implemented in this version of OpenRNG:

  - Beta
  - ChiSquare
  - Gamma
  - GaussianMV
  - Hypergeometric
  - Multinomial
  - Negbinomial
  - PoissonV
/*
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0 WITH LLVM-exception
 */

#pragma once

#include <map>
#include <vector>

namespace BinomialGolden {
struct key {
  int brng;
  int method;
  int seed;
  int ntrial;
  double p;
  auto operator<=>(const key &) const = default;
};

const std::map<key, std::vector<int>> reference = {
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10, 0.000100},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10, 0.010000},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10, 0.100000},
     {0, 0, 2, 0, 1, 0, 2, 0, 0, 1, 0, 1, 3, 1, 1, 1, 0, 2, 0, 1,
      0, 0, 2, 2, 0, 0, 0, 0, 1, 1, 2, 1, 2, 3, 0, 1, 1, 1, 2, 0,
      1, 2, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 2, 1, 2, 1, 1, 1, 0, 1,
      1, 1, 1, 1, 3, 0, 1, 1, 1, 1, 1, 0, 4, 1, 1, 0, 1, 2, 0, 0,
      0, 0, 2, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 2, 2, 1, 2, 0, 1, 1}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10, 0.500000},
     {0, 0, 6, 4, 4, 4, 7, 2, 4, 5, 2, 5, 8, 6, 6, 6, 4, 7, 4, 5,
      4, 3, 6, 7, 3, 2, 2, 2, 5, 5, 7, 5, 7, 8, 4, 5, 6, 5, 7, 3,
      5, 7, 5, 5, 5, 5, 3, 3, 5, 0, 2, 6, 6, 5, 7, 5, 5, 5, 2, 4,
      5, 5, 5, 5, 8, 1, 6, 5, 5, 4, 5, 3, 8, 6, 5, 4, 5, 7, 2, 2,
      4, 4, 7, 2, 6, 2, 4, 6, 4, 7, 4, 3, 2, 7, 6, 5, 6, 2, 5, 5}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 1000, 0.000100},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 1000, 0.010000},
     {0,  2,  13, 8,  9,  8,  14, 5,  7,  9,  5,  10, 16, 11, 11, 11, 7,
      14, 9,  10, 8,  6,  12, 14, 6,  5,  5,  4,  9,  11, 14, 9,  14, 16,
      8,  11, 11, 9,  13, 7,  10, 14, 11, 10, 10, 9,  7,  6,  11, 1,  4,
      12, 13, 11, 15, 9,  10, 10, 5,  9,  10, 9,  11, 9,  16, 3,  12, 9,
      9,  9,  10, 7,  18, 11, 10, 8,  11, 13, 5,  5,  7,  7,  14, 5,  11,
      5,  8,  11, 7,  14, 8,  6,  5,  14, 12, 10, 12, 5,  10, 9} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 1000, 0.100000},
     {100, 108, 108, 116, 98,  92,  119, 93,  92,  102, 107, 102, 101, 102, 79,
      97,  93,  114, 100, 89,  108, 102, 89,  89,  106, 95,  85,  91,  106, 113,
      111, 101, 103, 115, 95,  99,  91,  107, 87,  106, 110, 105, 86,  95,  100,
      85,  94,  114, 84,  106, 92,  84,  107, 114, 100, 113, 98,  99,  92,  100,
      105, 93,  111, 88,  108, 92,  105, 108, 86,  122, 124, 95,  105, 123, 98,
      87,  97,  104, 103, 121, 95,  98,  101, 98,  92,  93,  81,  97,  98,  104,
      98,  105, 109, 97,  116, 101, 112, 103, 108, 94}           },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 1000, 0.500000},
     {500, 493, 510, 516, 494, 483, 464, 507, 481, 500, 511, 502, 501, 499, 491,
      515, 511, 493, 513, 509, 502, 528, 478, 529, 505, 490, 514, 515, 507, 519,
      536, 514, 500, 501, 503, 512, 529, 494, 480, 510, 474, 488, 513, 507, 472,
      488, 493, 526, 486, 522, 469, 505, 509, 519, 518, 507, 508, 511, 497, 496,
      508, 495, 511, 528, 493, 509, 494, 507, 484, 504, 525, 494, 483, 485, 509,
      472, 457, 528, 506, 506, 459, 524, 521, 494, 506, 502, 486, 494, 495, 506,
      524, 517, 511, 477, 531, 489, 491, 504, 495, 504}          },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10000, 0.000100},
     {0, 0, 2, 0, 1, 0, 2, 0, 0, 1, 0, 1, 3, 1, 1, 1, 0, 2, 0, 1,
      0, 0, 2, 2, 0, 0, 0, 0, 1, 1, 2, 1, 2, 3, 0, 1, 1, 1, 2, 0,
      1, 2, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 2, 1, 3, 1, 1, 1, 0, 0,
      1, 1, 1, 1, 3, 0, 1, 1, 1, 0, 1, 0, 4, 1, 1, 0, 1, 2, 0, 0,
      0, 0, 2, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 2, 2, 1, 2, 0, 1, 1}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10000, 0.010000},
     {100, 108, 108, 116, 98,  91,  120, 91,  102, 108, 102, 101, 102, 78,  97,
      92,  115, 100, 88,  108, 102, 89,  88,  106, 95,  84,  90,  107, 114, 111,
      101, 103, 115, 95,  99,  90,  107, 86,  106, 111, 105, 85,  94,  99,  84,
      94,  115, 83,  107, 91,  83,  107, 115, 100, 114, 98,  99,  100, 105, 92,
      112, 88,  108, 91,  105, 108, 86,  123, 126, 94,  105, 124, 97,  86,  97,
      105, 103, 122, 94,  97,  101, 98,  92,  80,  97,  98,  104, 97,  106, 109,
      97,  116, 101, 113, 104, 108, 93,  101, 111, 93}           },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10000, 0.100000},
     {1000, 978,  1018, 1024, 989,  968,  932,  1012, 963,  999,  1019, 1004,
      1002, 996,  1035, 982,  1027, 1016, 986,  1023, 1015, 1004, 1052, 958,
      1052, 1008, 981,  1024, 1027, 1033, 1013, 1033, 1068, 1026, 998,  1001,
      1005, 1018, 1054, 987,  961,  1019, 950,  967,  1022, 1013, 947,  977,
      985,  1047, 973,  1040, 941,  1008, 1015, 1033, 1032, 1011, 1013, 1015,
      993,  991,  989,  1009, 990,  1019, 1049, 986,  1016, 987,  1013, 970,
      999,  1045, 980,  967,  962,  1017, 948,  919,  1051, 1010, 1009, 1061,
      1044, 1039, 988,  1011, 1002, 928,  972,  989,  989,  1009, 1044, 1030,
      1019, 945,  1056, 979}                                     },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_BINOMIAL_BTPE, 0, 10000, 0.500000},
     {5000, 4941, 5029, 5028, 4979, 4944, 5107, 5018, 4934, 4997, 5032, 5007,
      5003, 4991, 5049, 4967, 5044, 5012, 4974, 5037, 5024, 5007, 5087, 4927,
      4969, 5012, 4966, 5039, 5044, 5054, 5020, 5056, 5117, 5043, 4993, 5002,
      5007, 5016, 5091, 4976, 4931, 5031, 4913, 4895, 4920, 5036, 5022, 4906,
      4958, 4971, 5080, 4952, 5067, 4896, 5012, 5024, 5055, 5052, 5017, 5021,
      5011, 4987, 4982, 4961, 4981, 5029, 4974, 5025, 4975, 5021, 5029, 4947,
      4981, 5020, 5076, 4944, 4942, 4910, 5027, 4908, 4871, 4861, 5086, 5017,
      5013, 5098, 5073, 5065, 4978, 5018, 5003, 4875, 4950, 4981, 4979, 5012,
      5073, 5049, 5031, 5074}                                    }
};
} // namespace BinomialGolden
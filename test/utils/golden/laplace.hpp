/*
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0 WITH LLVM-exception
 */

#pragma once

#include <map>
#include <vector>

namespace LaplaceGolden {
struct key {
  int brngId;
  int method;
  int seed;
  double a;
  double beta;
  auto operator<=>(const key &) const = default;
};

const std::map<key, std::vector<float>> float_reference = {
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 0.000000, 1.000000},
     {0x1.472a5cp+5,  0x1.d584c4p-3,  0x1.fb49ep-1,   0x1.08b9ep-3,
      0x1.b62d5ap+0,  -0x1.9e0c22p+1, -0x1.7c83bep-5, -0x1.ca7d62p-2,
      -0x1.9a6f28p+0, 0x1.0e8102p+0,  0x1.31b20ap+0,  -0x1.f73472p-3,
      0x1.35ba7ep+1,  0x1.a0d6d6p+1,  -0x1.d29a06p-1, 0x1.b498fcp-4,
      -0x1.1441aep-3, -0x1.7260f8p+0, 0x1.c98638p-2,  0x1.2c53c8p-3,
      -0x1.81be34p-1, 0x1.01b192p-1,  0x1.6e8c42p-1,  0x1.02cbdap+1,
      0x1.feb78ap-2,  -0x1.dcb18ap+1, -0x1.90ad04p-3, 0x1.2aaf6ap-4,
      -0x1.6ec226p-1, 0x1.ac5fa2p+1,  0x1.24713ap-1,  0x1.ec66d4p-2,
      0x1.1b5d04p-5,  0x1.572c38p-2,  0x1.c7fa34p-1,  0x1.52638ep-1,
      -0x1.9451f4p-7, 0x1.57795ap-1,  -0x1.107554p-1, 0x1.8c6376p+1,
      0x1.9dab7ap+0,  0x1.235cacp-3,  0x1.a17a2ep-2,  -0x1.2ec9f4p+0,
      -0x1.b14472p+0, 0x1.4cf684p+0,  -0x1.934688p+1, -0x1.24e9d6p-2,
      0x1.05aeb8p-2,  0x1.36eb5ep-1,  0x1.96b98p+0,   -0x1.4f116ap+1,
      -0x1.f67ebap+0, -0x1.6ecef8p-1, 0x1.260fe8p-1,  -0x1.87f354p+0,
      0x1.736d28p-1,  -0x1.f8795cp+1, -0x1.390658p-1, -0x1.d7dc96p-2,
      0x1.6edb88p-2,  -0x1.65aa36p-3, 0x1.2c577ap-1,  -0x1.f6192cp-2,
      0x1.b6097p-1,   0x1.2f7976p-3,  0x1.97aa4ep+0,  -0x1.12699ap-1,
      -0x1.a43064p-3, 0x1.48328p-3,   -0x1.6b2aaep-9, 0x1.2491ap+1,
      -0x1.95824cp-2, 0x1.40618ap-4,  -0x1.0ac34ap+0, -0x1.d3428p-2,
      -0x1.b3be64p-1, 0x1.7a2b38p+0,  -0x1.28a8eap-2, -0x1.12d8bcp+1,
      0x1.7429dap-3,  -0x1.44d54ep-2, 0x1.084484p-1,  0x1.cce878p-3,
      -0x1.289192p+1, 0x1.11ac58p-2,  0x1.c38262p-1,  -0x1.6d74dcp+1,
      0x1.0ef6aap-5,  0x1.60c1dap-5,  0x1.a35d86p-2,  0x1.69af1ep+0,
      -0x1.ba818ap-2, 0x1.c43fa2p-5,  -0x1.09dd44p-2, 0x1.769508p-2,
      0x1.182b8ap-1,  0x1.33f234p+1,  0x1.ade6bep+0,  0x1.53e4eep+0} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 0.000000, 123.199997},
     {0x1.3ae592p+12, 0x1.c3e962p+4,  0x1.e843e6p+6,  0x1.fd9902p+3,
      0x1.a5bedap+7,  -0x1.8e8546p+8, -0x1.6e3eccp+2, -0x1.b94bep+5,
      -0x1.8b0afcp+7, 0x1.045c2cp+7,  0x1.263b5cp+7,  -0x1.e455acp+4,
      0x1.2a1d18p+8,  0x1.91352ep+8,  -0x1.c11aa6p+6, 0x1.a439a6p+3,
      -0x1.09e59ep+4, -0x1.647d54p+7, 0x1.b85dfcp+5,  0x1.2110a4p+4,
      -0x1.734712p+6, 0x1.f00f6cp+5,  0x1.60cd66p+6,  0x1.f22edp+7,
      0x1.eb90a8p+5,  -0x1.cad148p+8, -0x1.81a686p+4, 0x1.1f7c08p+3,
      -0x1.610144p+6, 0x1.9c4f3ep+8,  0x1.1979c8p+6,  0x1.d9efc4p+5,
      0x1.10bcbap+2,  0x1.4a4dc2p+5,  0x1.b6e0d2p+6,  0x1.45b304p+6,
      -0x1.85287ap+0, 0x1.4a98p+6,    -0x1.063dbap+6, 0x1.7d862p+8,
      0x1.8e283ep+7,  0x1.186f98p+4,  0x1.91d266p+5,  -0x1.236f2ep+7,
      -0x1.a10514p+7, 0x1.407a12p+7,  -0x1.842716p+8, -0x1.19eddep+5,
      0x1.f7bd22p+4,  0x1.2b428ap+6,  0x1.8778fp+7,   -0x1.4280c2p+8,
      -0x1.e3a6c6p+7, -0x1.610d9ap+6, 0x1.1b08e8p+6,  -0x1.79409ap+7,
      0x1.657f76p+6,  -0x1.e58e68p+8, -0x1.2d494ep+6, -0x1.c62ab6p+5,
      0x1.6119b2p+5,  -0x1.5840ap+4,  0x1.211432p+6,  -0x1.e34506p+5,
      0x1.a59c48p+6,  0x1.24181ap+4,  0x1.8860b8p+7,  -0x1.081f3ep+6,
      -0x1.946e92p+4, 0x1.3be3cep+4,  -0x1.5d8c46p-2, 0x1.1998f6p+8,
      -0x1.864d68p+5, 0x1.345de2p+3,  -0x1.00c25ep+7, -0x1.c1bccep+5,
      -0x1.a3674p+6,  0x1.6bfcccp+7,  -0x1.1d88fap+5, -0x1.088a34p+8,
      0x1.663514p+4,  -0x1.38a6e8p+5, 0x1.fcb716p+5,  0x1.bb9fcp+4,
      -0x1.1d7282p+8, 0x1.076914p+5,  0x1.b293e4p+6,  -0x1.5fc07ap+8,
      0x1.04cd6ap+2,  0x1.538762p+2,  0x1.93a39ep+5,  0x1.5c1ef2p+7,
      -0x1.a9e97ap+5, 0x1.b34a0cp+2,  -0x1.ffc9eep+4, 0x1.68890ap+5,
      0x1.0da9e8p+6,  0x1.2865ecp+8,  0x1.9dc7bp+7,   0x1.4725f2p+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 1.000000, 1.000000},
     {0x1.4f2a5cp+5,  0x1.3ab098p+0,  0x1.fda4fp+0,   0x1.21173cp+0,
      0x1.5b16acp+1,  -0x1.1e0c22p+1, 0x1.e837c4p-1,  0x1.1ac15p-1,
      -0x1.34de5p-1,  0x1.07408p+1,   0x1.18d904p+1,  0x1.8232e4p-1,
      0x1.b5ba7ep+1,  0x1.106b6cp+2,  0x1.6b2fdp-4,   0x1.1b499p+0,
      0x1.baef94p-1,  -0x1.c983ep-2,  0x1.72618ep+0,  0x1.258a78p+0,
      0x1.f9073p-3,   0x1.80d8c8p+0,  0x1.b7462p+0,   0x1.82cbdap+1,
      0x1.7fade2p+0,  -0x1.5cb18ap+1, 0x1.9bd4cp-1,   0x1.12aaf6p+0,
      0x1.227bb4p-2,  0x1.162fdp+2,   0x1.92389cp+0,  0x1.7b19b4p+0,
      0x1.08dae8p+0,  0x1.55cb0ep+0,  0x1.e3fd1ap+0,  0x1.a931c8p+0,
      0x1.f9aeb8p-1,  0x1.abbcacp+0,  0x1.df1558p-2,  0x1.0631bcp+2,
      0x1.4ed5bcp+1,  0x1.246b96p+0,  0x1.685e8cp+0,  -0x1.764fap-3,
      -0x1.6288e4p-1, 0x1.267b42p+1,  -0x1.134688p+1, 0x1.6d8b14p-1,
      0x1.416baep+0,  0x1.9b75bp+0,   0x1.4b5ccp+1,   -0x1.9e22d4p+0,
      -0x1.ecfd74p-1, 0x1.22621p-2,   0x1.9307f4p+0,  -0x1.0fe6a8p-1,
      0x1.b9b694p+0,  -0x1.78795cp+1, 0x1.8df35p-2,   0x1.1411b4p-1,
      0x1.5bb6e2p+0,  0x1.a69572p-1,  0x1.962bbcp+0,  0x1.04f36ap-1,
      0x1.db04b8p+0,  0x1.25ef2ep+0,  0x1.4bd528p+1,  0x1.db2cccp-2,
      0x1.96f3e8p-1,  0x1.29065p+0,   0x1.fe94d6p-1,  0x1.a491ap+1,
      0x1.353edap-1,  0x1.140618p+0,  -0x1.58694p-5,  0x1.165ecp-1,
      0x1.31067p-3,   0x1.3d159cp+1,  0x1.6bab8cp-1,  -0x1.25b178p+0,
      0x1.2e853cp+0,  0x1.5d9558p-1,  0x1.842242p+0,  0x1.399d1p+0,
      -0x1.512324p+0, 0x1.446b16p+0,  0x1.e1c13p+0,   -0x1.dae9b8p+0,
      0x1.0877b6p+0,  0x1.0b060ep+0,  0x1.68d762p+0,  0x1.34d79p+1,
      0x1.22bf3cp-1,  0x1.0e21fep+0,  0x1.7b115ep-1,  0x1.5da542p+0,
      0x1.8c15c4p+0,  0x1.b3f234p+1,  0x1.56f36p+1,   0x1.29f278p+1} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 1.000000, 123.199997},
     {0x1.3af592p+12, 0x1.d3e962p+4,  0x1.ec43e6p+6,  0x1.0ecc8p+4,
      0x1.a7bedap+7,  -0x1.8d8546p+8, -0x1.2e3eccp+2, -0x1.b14bep+5,
      -0x1.890afcp+7, 0x1.065c2cp+7,  0x1.283b5cp+7,  -0x1.d455acp+4,
      0x1.2b1d18p+8,  0x1.92352ep+8,  -0x1.bd1aa6p+6, 0x1.c439a6p+3,
      -0x1.f3cb3ap+3, -0x1.627d54p+7, 0x1.c05dfcp+5,  0x1.3110a4p+4,
      -0x1.6f4712p+6, 0x1.f80f6cp+5,  0x1.64cd66p+6,  0x1.f42edp+7,
      0x1.f390a8p+5,  -0x1.c9d148p+8, -0x1.71a686p+4, 0x1.3f7c08p+3,
      -0x1.5d0144p+6, 0x1.9d4f3ep+8,  0x1.1d79c8p+6,  0x1.e1efc4p+5,
      0x1.50bcbap+2,  0x1.524dc2p+5,  0x1.bae0d2p+6,  0x1.49b304p+6,
      -0x1.0a50f4p-1, 0x1.4e98p+6,    -0x1.023dbap+6, 0x1.7e862p+8,
      0x1.90283ep+7,  0x1.286f98p+4,  0x1.99d266p+5,  -0x1.216f2ep+7,
      -0x1.9f0514p+7, 0x1.427a12p+7,  -0x1.832716p+8, -0x1.11eddep+5,
      0x1.03de9p+5,   0x1.2f428ap+6,  0x1.8978fp+7,   -0x1.4180c2p+8,
      -0x1.e1a6c6p+7, -0x1.5d0d9ap+6, 0x1.1f08e8p+6,  -0x1.77409ap+7,
      0x1.697f76p+6,  -0x1.e48e68p+8, -0x1.29494ep+6, -0x1.be2ab6p+5,
      0x1.6919b2p+5,  -0x1.4840ap+4,  0x1.251432p+6,  -0x1.db4506p+5,
      0x1.a99c48p+6,  0x1.34181ap+4,  0x1.8a60b8p+7,  -0x1.041f3ep+6,
      -0x1.846e92p+4, 0x1.4be3cep+4,  0x1.5139dcp-1,  0x1.1a98f6p+8,
      -0x1.7e4d68p+5, 0x1.545de2p+3,  -0x1.fd84bap+6, -0x1.b9bccep+5,
      -0x1.9f674p+6,  0x1.6dfcccp+7,  -0x1.1588fap+5, -0x1.078a34p+8,
      0x1.763514p+4,  -0x1.30a6e8p+5, 0x1.025b8cp+6,  0x1.cb9fcp+4,
      -0x1.1c7282p+8, 0x1.0f6914p+5,  0x1.b693e4p+6,  -0x1.5ec07ap+8,
      0x1.44cd6ap+2,  0x1.938762p+2,  0x1.9ba39ep+5,  0x1.5e1ef2p+7,
      -0x1.a1e97ap+5, 0x1.f34a0cp+2,  -0x1.efc9eep+4, 0x1.70890ap+5,
      0x1.11a9e8p+6,  0x1.2965ecp+8,  0x1.9fc7bp+7,   0x1.4925f2p+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, -2.000000, 1.000000},
     {0x1.372a5cp+5,  -0x1.c54f68p+0, -0x1.025b1p+0,  -0x1.dee8c4p+0,
      -0x1.274a98p-2, -0x1.4f061p+2,  -0x1.05f20ep+1, -0x1.394facp+1,
      -0x1.cd3794p+1, -0x1.e2fdfcp-1, -0x1.9c9becp-1, -0x1.1f7348p+1,
      0x1.add3fp-2,   0x1.41adacp+0,  -0x1.74a682p+1, -0x1.e4b67p+0,
      -0x1.11441ap+1, -0x1.b9307cp+1, -0x1.8d9e72p+0, -0x1.da7588p+0,
      -0x1.606f8cp+1, -0x1.7f2738p+0, -0x1.48b9ep+0,  0x1.65edp-6,
      -0x1.80521ep+0, -0x1.6e58c4p+2, -0x1.190adp+1,  -0x1.ed550ap+0,
      -0x1.5bb08ap+1, 0x1.58bf44p+0,  -0x1.6dc764p+0, -0x1.84e64cp+0,
      -0x1.f72518p+0, -0x1.aa34f2p+0, -0x1.1c02e6p+0, -0x1.56ce38p+0,
      -0x1.019452p+1, -0x1.544354p+0, -0x1.441d54p+1, 0x1.18c6ecp+0,
      -0x1.895218p-2, -0x1.db946ap+0, -0x1.97a174p+0, -0x1.9764fap+1,
      -0x1.d8a238p+1, -0x1.6612f8p-1, -0x1.49a344p+2, -0x1.249d3ap+1,
      -0x1.be9452p+0, -0x1.648a5p+0,  -0x1.a51ap-2,   -0x1.2788b4p+2,
      -0x1.fb3f5cp+1, -0x1.5bb3bep+1, -0x1.6cf80cp+0, -0x1.c3f9aap+1,
      -0x1.46496cp+0, -0x1.7c3caep+2, -0x1.4e4196p+1, -0x1.3afb92p+1,
      -0x1.a4491ep+0, -0x1.165aa4p+1, -0x1.69d444p+0, -0x1.3ec326p+1,
      -0x1.24fb48p+0, -0x1.da10d2p+0, -0x1.a156c8p-2, -0x1.449a66p+1,
      -0x1.1a4306p+1, -0x1.d6f9bp+0,  -0x1.005acap+1, 0x1.248dp-2,
      -0x1.32b04ap+1, -0x1.ebf9e8p+0, -0x1.8561a4p+1, -0x1.3a685p+1,
      -0x1.6cef98p+1, -0x1.0ba99p-1,  -0x1.25151ep+1, -0x1.096c5ep+2,
      -0x1.d17ac4p+0, -0x1.289aaap+1, -0x1.7bddbep+0, -0x1.c662fp+0,
      -0x1.1448c8p+2, -0x1.bb94eap+0, -0x1.1e3edp+0,  -0x1.36ba6ep+2,
      -0x1.f7884ap+0, -0x1.f4f9f2p+0, -0x1.97289ep+0, -0x1.2ca1c4p-1,
      -0x1.375032p+1, -0x1.f1de02p+0, -0x1.213ba8p+1, -0x1.a25abep+0,
      -0x1.73ea3cp+0, 0x1.9f91ap-2,   -0x1.486508p-2, -0x1.583624p-1}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, -2.000000, 123.199997},
     {0x1.3ac592p+12, 0x1.a3e962p+4,  0x1.e043e6p+6,  0x1.bd9902p+3,
      0x1.a1bedap+7,  -0x1.908546p+8, -0x1.ee3eccp+2, -0x1.c94bep+5,
      -0x1.8f0afcp+7, 0x1.005c2cp+7,  0x1.223b5cp+7,  -0x1.022ad6p+5,
      0x1.281d18p+8,  0x1.8f352ep+8,  -0x1.c91aa6p+6, 0x1.6439a6p+3,
      -0x1.29e59ep+4, -0x1.687d54p+7, 0x1.a85dfcp+5,  0x1.0110a4p+4,
      -0x1.7b4712p+6, 0x1.e00f6cp+5,  0x1.58cd66p+6,  0x1.ee2edp+7,
      0x1.db90a8p+5,  -0x1.ccd148p+8, -0x1.a1a686p+4, 0x1.bef812p+2,
      -0x1.690144p+6, 0x1.9a4f3ep+8,  0x1.1179c8p+6,  0x1.c9efc4p+5,
      0x1.217974p+1,  0x1.3a4dc2p+5,  0x1.aee0d2p+6,  0x1.3db304p+6,
      -0x1.c2943ep+1, 0x1.4298p+6,    -0x1.0e3dbap+6, 0x1.7b862p+8,
      0x1.8a283ep+7,  0x1.f0df3p+3,   0x1.81d266p+5,  -0x1.276f2ep+7,
      -0x1.a50514p+7, 0x1.3c7a12p+7,  -0x1.862716p+8, -0x1.29eddep+5,
      0x1.d7bd22p+4,  0x1.23428ap+6,  0x1.8378fp+7,   -0x1.4480c2p+8,
      -0x1.e7a6c6p+7, -0x1.690d9ap+6, 0x1.1308e8p+6,  -0x1.7d409ap+7,
      0x1.5d7f76p+6,  -0x1.e78e68p+8, -0x1.35494ep+6, -0x1.d62ab6p+5,
      0x1.5119b2p+5,  -0x1.7840ap+4,  0x1.191432p+6,  -0x1.f34506p+5,
      0x1.9d9c48p+6,  0x1.04181ap+4,  0x1.8460b8p+7,  -0x1.101f3ep+6,
      -0x1.b46e92p+4, 0x1.1be3cep+4,  -0x1.2bb188p+1, 0x1.1798f6p+8,
      -0x1.964d68p+5, 0x1.e8bbc2p+2,  -0x1.04c25ep+7, -0x1.d1bccep+5,
      -0x1.ab674p+6,  0x1.67fcccp+7,  -0x1.2d88fap+5, -0x1.0a8a34p+8,
      0x1.463514p+4,  -0x1.48a6e8p+5, 0x1.ecb716p+5,  0x1.9b9fcp+4,
      -0x1.1f7282p+8, 0x1.eed228p+4,  0x1.aa93e4p+6,  -0x1.61c07ap+8,
      0x1.099ad4p+1,  0x1.a70ec2p+1,  0x1.83a39ep+5,  0x1.581ef2p+7,
      -0x1.b9e97ap+5, 0x1.334a0cp+2,  -0x1.0fe4f8p+5, 0x1.58890ap+5,
      0x1.05a9e8p+6,  0x1.2665ecp+8,  0x1.99c7bp+7,   0x1.4325f2p+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 123.199997, 1.000000},
     {0x1.4830fcp+7, 0x1.edb78ep+6, 0x1.f0c36p+6,  0x1.ed5128p+6,
      0x1.f3a582p+6, 0x1.dfdc6ap+6, 0x1.ec9d3cp+6, 0x1.eb024ep+6,
      0x1.e6631p+6,  0x1.f106dp+6,  0x1.f19394p+6, 0x1.ebd132p+6,
      0x1.f67aap+6,  0x1.f9d382p+6, 0x1.e92798p+6, 0x1.ed39f2p+6,
      0x1.ec42acp+6, 0x1.e70348p+6, 0x1.ee9652p+6, 0x1.ed62f6p+6,
      0x1.e9c95p+6,  0x1.eed03p+6,  0x1.efa9e4p+6, 0x1.f4e32ap+6,
      0x1.eecb84p+6, 0x1.dde74p+6,  0x1.ec0476p+6, 0x1.ed1778p+6,
      0x1.e9ef48p+6, 0x1.fa2fcap+6, 0x1.ef15aep+6, 0x1.eeb932p+6,
      0x1.ecf038p+6, 0x1.ee23f8p+6, 0x1.f05ccp+6,  0x1.ef7194p+6,
      0x1.ecc02ap+6, 0x1.ef7bbep+6, 0x1.eaabe2p+6, 0x1.f92fe8p+6,
      0x1.f3437ap+6, 0x1.ed5e7ap+6, 0x1.ee6e46p+6, 0x1.e811a4p+6,
      0x1.e607bap+6, 0x1.f200a6p+6, 0x1.e03298p+6, 0x1.eba7e2p+6,
      0x1.edd27ap+6, 0x1.ef3aa2p+6, 0x1.f327b2p+6, 0x1.e2544p+6,
      0x1.e4f2d2p+6, 0x1.e9ef2ep+6, 0x1.ef18ecp+6, 0x1.e6acfep+6,
      0x1.efb3a6p+6, 0x1.dd0902p+6, 0x1.ea5acp+6,  0x1.eaf4fp+6,
      0x1.ee3ba8p+6, 0x1.ec19f6p+6, 0x1.ef257ap+6, 0x1.ead6b2p+6,
      0x1.f038dep+6, 0x1.ed6488p+6, 0x1.f32b76p+6, 0x1.eaa7f8p+6,
      0x1.ebfab4p+6, 0x1.ed70e6p+6, 0x1.ecc9f6p+6, 0x1.f5f158p+6,
      0x1.eb374ap+6, 0x1.ed1ce4p+6, 0x1.e8a1bep+6, 0x1.eaf98ap+6,
      0x1.e9655p+6,  0x1.f2b578p+6, 0x1.eba424p+6, 0x1.e43606p+6,
      0x1.ed86ep+6,  0x1.eb87f6p+6, 0x1.eedd56p+6, 0x1.edb34p+6,
      0x1.e3884p+6,  0x1.edde78p+6, 0x1.f053dp+6,  0x1.e16126p+6,
      0x1.eceeaap+6, 0x1.ecf8e4p+6, 0x1.ee702ap+6, 0x1.f27388p+6,
      0x1.eb124ap+6, 0x1.ed0554p+6, 0x1.ebc2eep+6, 0x1.ee4362p+6,
      0x1.eefd24p+6, 0x1.f66c5ep+6, 0x1.f38466p+6, 0x1.f21c6p+6}     },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 123.199997, 123.199997},
     {0x1.4298c4p+12, 0x1.2ee392p+7,  0x1.ea885ap+7,  0x1.163ff6p+7,
      0x1.4e12ap+8,   -0x1.135214p+8, 0x1.d5e8ep+6,   0x1.1026dcp+6,
      -0x1.29492cp+6, 0x1.fac292p+7,  0x1.0e50e2p+8,  0x1.73b76p+6,
      0x1.a5504cp+8,  0x1.06343p+9,   0x1.5d9138p+3,  0x1.10aap+7,
      0x1.aa5364p+6,  -0x1.b85bbap+5, 0x1.647de4p+7,  0x1.1a887ap+7,
      0x1.e616eap+4,  0x1.726a4p+7,   0x1.a6cd18p+7,  0x1.744a9ap+8,
      0x1.714a9p+7,   -0x1.4f9e14p+8, 0x1.8c632ap+6,  0x1.085e26p+7,
      0x1.17971p+5,   0x1.0bc138p+9,  0x1.83234ap+7,  0x1.6ce258p+7,
      0x1.fdd898p+6,  0x1.48f9d6p+7,  0x1.d1d6cep+7,  0x1.993fe8p+7,
      0x1.e6b82ap+6,  0x1.9bb266p+7,  0x1.cd1e24p+5,  0x1.f8b954p+8,
      0x1.424752p+8,  0x1.19745ap+7,  0x1.5adbp+7,    -0x1.68463cp+4,
      -0x1.553d5ap+6, 0x1.1b703cp+8,  -0x1.08f3e2p+8, 0x1.5fd5dep+6,
      0x1.355e0ap+7,  0x1.8c07aap+7,  0x1.3eefacp+8,  -0x1.8e9b1ep+7,
      -0x1.da80bep+6, 0x1.177e62p+5,  0x1.83eadap+7,  -0x1.05b468p+6,
      0x1.a9262p+7,   -0x1.6a5b34p+8, 0x1.7f06fcp+5,  0x1.09b77p+6,
      0x1.4eacd2p+7,  0x1.96bca4p+6,  0x1.86f07ep+7,  0x1.f65492p+5,
      0x1.c9348ap+7,  0x1.1ae96ap+7,  0x1.3f638ep+8,  0x1.c95b1ep+5,
      0x1.87b128p+6,  0x1.1de2ep+7,   0x1.eb6f4p+6,   0x1.94cc2ap+8,
      0x1.29a618p+6,  0x1.09ac44p+7,  -0x1.4b7ee6p+2, 0x1.0bee66p+6,
      0x1.259632p+4,  0x1.313198p+8,  0x1.5e084ep+6,  -0x1.1aae04p+7,
      0x1.232d08p+7,  0x1.507958p+6,  0x1.75942cp+7,  0x1.2dda5ep+7,
      -0x1.447e9ep+7, 0x1.3840acp+7,  0x1.cfb058p+7,  -0x1.c91a8ep+7,
      0x1.fd19a2p+6,  0x1.0102a2p+7,  0x1.5b4f4ep+7,  0x1.2942acp+8,
      0x1.17d80ep+6,  0x1.0400b6p+7,  0x1.6cda5p+6,   0x1.5088a8p+7,
      0x1.7d3b5ap+7,  0x1.a3991ep+8,  0x1.4a170ap+8,  0x1.1ec62cp+8} }
};

const std::map<key, std::vector<double>> double_reference = {
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 0.000000, 1.000000},
     {0x1.472a5c30ead69p+5,  0x1.d584c886ce618p-3,  0x1.fb49e200a48a4p-1,
      0x1.08b9db921b83dp-3,  0x1.b62d58fde26b4p+0,  -0x1.9e0c219cae587p+1,
      -0x1.7c83ba6df083bp-5, -0x1.ca7d62e4cc349p-2, -0x1.9a6f27f19b8c5p+0,
      0x1.0e8100fd74146p+0,  0x1.31b209dbfdd99p+0,  -0x1.f73474ec7edbep-3,
      0x1.35ba7e19eb857p+1,  0x1.a0d6d5f889fa2p+1,  -0x1.d29a05f9a26d5p-1,
      0x1.b4990088ff344p-4,  -0x1.1441ae919b349p-3, -0x1.7260f813ee2efp+0,
      0x1.c986350cd67cep-2,  0x1.2c53c96c4ce7fp-3,  -0x1.81be343eff4a3p-1,
      0x1.01b19304ed1bap-1,  0x1.6e8c41e75010bp-1,  0x1.02cbd9204b35dp+1,
      0x1.feb78cd9afddbp-2,  -0x1.dcb18957d98b5p+1, -0x1.90ad01855304bp-3,
      0x1.2aaf6493a139p-4,   -0x1.6ec22499c30b2p-1, 0x1.ac5fa246cbaa3p+1,
      0x1.24713816ccb0ep-1,  0x1.ec66d0e6b5816p-2,  0x1.1b5cf56fdf9bfp-5,
      0x1.572c39d220204p-2,  0x1.c7fa34b2a115bp-1,  0x1.52638cfc9cebdp-1,
      -0x1.9451e0afade28p-7, 0x1.5779580158df3p-1,  -0x1.107554e456219p-1,
      0x1.8c6375a661771p+1,  0x1.9dab7a59c0938p+0,  0x1.235cae9613d53p-3,
      0x1.a17a2f67e0096p-2,  -0x1.2ec9f464981eap+0, -0x1.b14471b5ee1f2p+0,
      0x1.4cf684292e49fp+0,  -0x1.934688e125827p+1, -0x1.24e9d60b1088ap-2,
      0x1.05aeb5d1161cbp-2,  0x1.36eb5e2b5ac0bp-1,  0x1.96b980cfa0d0dp+0,
      -0x1.4f116993f0de7p+1, -0x1.f67eb942c8bdep+0, -0x1.6ecef7f66f388p-1,
      0x1.260fe6cdd4ed4p-1,  -0x1.87f35383155f4p+0, 0x1.736d27679936cp-1,
      -0x1.f8795b2ab8a87p+1, -0x1.390659501eb73p-1, -0x1.d7dc935166769p-2,
      0x1.6edb85c3b112ep-2,  -0x1.65aa323a52911p-3, 0x1.2c577973206bdp-1,
      -0x1.f6192c11f8686p-2, 0x1.b6097068b51a6p-1,  0x1.2f797af444e86p-3,
      0x1.97aa4d6f99816p+0,  -0x1.1269990664bd1p-1, -0x1.a4306417b61dap-3,
      0x1.483283156f163p-3,  -0x1.6b2aec4fac2d1p-9, 0x1.24919f67c8dc4p+1,
      -0x1.95824ba130f45p-2, 0x1.406186f9fff3dp-4,  -0x1.0ac34a1afb3c2p+0,
      -0x1.d3428262a7f0cp-2, -0x1.b3be63e44d105p-1, 0x1.7a2b370bd8edep+0,
      -0x1.28a8e85b1ce34p-2, -0x1.12d8bb5798839p+1, 0x1.7429d7236e52fp-3,
      -0x1.44d54e3d244cap-2, 0x1.0844824d06661p-1,  0x1.cce87bc4decb5p-3,
      -0x1.28919277f6bc8p+1, 0x1.11ac548b5b13fp-2,  0x1.c38260d1cb17p-1,
      -0x1.6d74dcc7ecbe7p+1, 0x1.0ef6aeb75dc04p-5,  0x1.60c1df26284abp-5,
      0x1.a35d858ebf9f2p-2,  0x1.69af1e21969dfp+0,  -0x1.ba81878a25f3ap-2,
      0x1.c43faefcc4a3ep-5,  -0x1.09dd44abcd8b4p-2, 0x1.76950521c9d21p-2,
      0x1.182b8ab56b45cp-1,  0x1.33f2334ae3868p+1,  0x1.ade6bdc517d6cp+0,
      0x1.53e4ece8b30eep+0} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 0.000000, 123.200000},
     {0x1.3ae592557ba1cp+12, 0x1.c3e9676826a44p+4,  0x1.e843e98704c51p+6,
      0x1.fd98f9dfa82a9p+3,  0x1.a5bed8dac3874p+7,  -0x1.8e8546c067cecp+8,
      -0x1.6e3ec9d69de53p+2, -0x1.b94be2629e25dp+5, -0x1.8b0afcd88c1d7p+7,
      0x1.045c2a8d8c86dp+7,  0x1.263b5cb08abbp+7,   -0x1.e455b08a06e6dp+4,
      0x1.2a1d195f5916dp+8,  0x1.91352df26b33fp+8,  -0x1.c11aa5c045efap+6,
      0x1.a439aa1d75a25p+3,  -0x1.09e59e6c2562ap+4, -0x1.647d552cc8739p+7,
      0x1.b85df975f4d83p+5,  0x1.2110a511d6d27p+4,  -0x1.734712496f51p+6,
      0x1.f00f6e36486edp+5,  0x1.60cd65d509dcep+6,  0x1.f22ecec490c7ap+7,
      0x1.eb90aac4b9456p+5,  -0x1.cad147648e2fbp+8, -0x1.81a684a9ecb48p+4,
      0x1.1f7c04014b2d4p+3,  -0x1.6101433a65545p+6, 0x1.9c4f3f642407p+8,
      0x1.1979c5fc58371p+6,  0x1.d9efc2aadb7fcp+5,  0x1.10bcac3bad9fbp+2,
      0x1.4a4dc473d8857p+5,  0x1.b6e0d2b8bb0b5p+6,  0x1.45b3047ff0a3p+6,
      -0x1.85286842b0f6dp+0, 0x1.4a97fe4e18bd4p+6,  -0x1.063dbb4ef94d2p+6,
      0x1.7d86213cf102ap+8,  0x1.8e283f5cc95acp+7,  0x1.186f9b3d3fe3ap+4,
      0x1.91d2673a613c4p+5,  -0x1.236f2e6d9f371p+7, -0x1.a10513d8b5313p+7,
      0x1.407a1267a2f4p+7,   -0x1.842716f24db3fp+8, -0x1.19edde043fe9fp+5,
      0x1.f7bd1dff4a90dp+4,  0x1.2b428aa354264p+6,  0x1.8778f261712f6p+7,
      -0x1.4280c26b316fbp+8, -0x1.e3a6c5837ad06p+7, -0x1.610d9b76cb0cdp+6,
      0x1.1b08e7bfb68acp+6,  -0x1.794099faf7c55p+7, 0x1.657f75ed5044bp+6,
      -0x1.e58e67bf84eefp+8, -0x1.2d494f9050c38p+6, -0x1.c62ab4318c388p+5,
      0x1.6119b0bf8da23p+5,  -0x1.58409d24ef787p+4, 0x1.211431b20267cp+6,
      -0x1.e3450737b24aep+5, 0x1.a59c48fe6183p+6,   0x1.24181ff18252ep+4,
      0x1.8860b75503bfcp+7,  -0x1.081f3ce2f4293p+6, -0x1.946e938a05afcp+4,
      0x1.3be3d15e3aebcp+4,  -0x1.5d8c837315b83p-2, 0x1.1998f63a4aedap+8,
      -0x1.864d68cb2584cp+5, 0x1.345ddeb7065abp+3,  -0x1.00c25db9f836bp+7,
      -0x1.c1bcd0b2280afp+5, -0x1.a367402556f95p+6, 0x1.6bfccb61cd983p+7,
      -0x1.1d88f93e189acp+5, -0x1.088a34511c651p+8, 0x1.663512454d631p+4,
      -0x1.38a6e81ad9569p+5, 0x1.fcb7146ddf848p+5,  0x1.bb9fc3ed7cd6ep+4,
      -0x1.1d72836043e24p+8, 0x1.0769115fbadcdp+5,  0x1.b293e396b9dfcp+6,
      -0x1.5fc07ae6d3ddbp+8, 0x1.04cd6e907d6f7p+2,  0x1.538766c7ed2e5p+2,
      0x1.93a39d59653c6p+5,  0x1.5c1ef366ba91ap+7,  -0x1.a9e978db5e20ep+5,
      0x1.b34a186ce3aa9p+2,  -0x1.ffc9f0fdebac1p+4, 0x1.68890823b8a7p+5,
      0x1.0da9e8b5040ccp+6,  0x1.2865eaf81497ep+8,  0x1.9dc7b040e6f1fp+7,
      0x1.4725f0d32c57fp+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 1.000000, 1.000000},
     {0x1.4f2a5c30ead69p+5,  0x1.3ab09910d9cc3p+0,  0x1.fda4f10052452p+0,
      0x1.21173b7243708p+0,  0x1.5b16ac7ef135ap+1,  -0x1.1e0c219cae587p+1,
      0x1.e837c45920f7cp-1,  0x1.1ac14e8d99e5cp-1,  -0x1.34de4fe33718ap-1,
      0x1.0740807eba0a3p+1,  0x1.18d904edfeeccp+1,  0x1.8232e2c4e049p-1,
      0x1.b5ba7e19eb857p+1,  0x1.106b6afc44fd1p+2,  0x1.6b2fd032ec958p-4,
      0x1.1b4990088ff34p+0,  0x1.baef945b9932ep-1,  -0x1.c983e04fb8bbcp-2,
      0x1.72618d43359f4p+0,  0x1.258a792d899dp+0,   0x1.f9072f0402d74p-3,
      0x1.80d8c982768ddp+0,  0x1.b74620f3a8086p+0,  0x1.82cbd9204b35dp+1,
      0x1.7fade3366bf77p+0,  -0x1.5cb18957d98b5p+1, 0x1.9bd4bf9eab3edp-1,
      0x1.12aaf6493a139p+0,  0x1.227bb6cc79e9cp-2,  0x1.162fd12365d52p+2,
      0x1.92389c0b66587p+0,  0x1.7b19b439ad606p+0,  0x1.08dae7ab7efcep+0,
      0x1.55cb0e7488081p+0,  0x1.e3fd1a59508aep+0,  0x1.a931c67e4e75ep+0,
      0x1.f9aeb87d41487p-1,  0x1.abbcac00ac6fap+0,  0x1.df15563753bcep-2,
      0x1.0631bad330bb8p+2,  0x1.4ed5bd2ce049cp+1,  0x1.246b95d2c27aap+0,
      0x1.685e8bd9f8026p+0,  -0x1.764fa324c0f5p-3,  -0x1.6288e36bdc3e4p-1,
      0x1.267b42149725p+1,   -0x1.134688e125827p+1, 0x1.6d8b14fa77bbbp-1,
      0x1.416bad7445873p+0,  0x1.9b75af15ad606p+0,  0x1.4b5cc067d0686p+1,
      -0x1.9e22d327e1bcep+0, -0x1.ecfd7285917bcp-1, 0x1.22621013218fp-2,
      0x1.9307f366ea76ap+0,  -0x1.0fe6a7062abe8p-1, 0x1.b9b693b3cc9b6p+0,
      -0x1.78795b2ab8a87p+1, 0x1.8df34d5fc291ap-2,  0x1.1411b6574cc4cp-1,
      0x1.5bb6e170ec44cp+0,  0x1.a69573716b5bcp-1,  0x1.962bbcb99035ep+0,
      0x1.04f369f703cbdp-1,  0x1.db04b8345a8d3p+0,  0x1.25ef2f5e889d1p+0,
      0x1.4bd526b7ccc0bp+1,  0x1.db2ccdf33685ep-2,  0x1.96f3e6fa1278ap-1,
      0x1.29065062ade2cp+0,  0x1.fe94d513b053dp-1,  0x1.a4919f67c8dc4p+1,
      0x1.353eda2f6785ep-1,  0x1.1406186f9fff4p+0,  -0x1.5869435f6784p-5,
      0x1.165ebeceac07ap-1,  0x1.3106706ecbbecp-3,  0x1.3d159b85ec76fp+1,
      0x1.6bab8bd2718e6p-1,  -0x1.25b176af31072p+0, 0x1.2e853ae46dca6p+0,
      0x1.5d9558e16dd9bp-1,  0x1.842241268333p+0,   0x1.399d0f789bd97p+0,
      -0x1.512324efed79p+0,  0x1.446b1522d6c5p+0,   0x1.e1c13068e58b8p+0,
      -0x1.dae9b98fd97cep+0, 0x1.0877b575baeep+0,   0x1.0b060ef931425p+0,
      0x1.68d76163afe7cp+0,  0x1.34d78f10cb4fp+1,   0x1.22bf3c3aed063p-1,
      0x1.0e21fd77e6252p+0,  0x1.7b115daa193a6p-1,  0x1.5da5414872748p+0,
      0x1.8c15c55ab5a2ep+0,  0x1.b3f2334ae3868p+1,  0x1.56f35ee28beb6p+1,
      0x1.29f2767459877p+1} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 1.000000, 123.200000},
     {0x1.3af592557ba1cp+12, 0x1.d3e9676826a44p+4,  0x1.ec43e98704c51p+6,
      0x1.0ecc7cefd4154p+4,  0x1.a7bed8dac3874p+7,  -0x1.8d8546c067cecp+8,
      -0x1.2e3ec9d69de53p+2, -0x1.b14be2629e25dp+5, -0x1.890afcd88c1d7p+7,
      0x1.065c2a8d8c86dp+7,  0x1.283b5cb08abbp+7,   -0x1.d455b08a06e6dp+4,
      0x1.2b1d195f5916dp+8,  0x1.92352df26b33fp+8,  -0x1.bd1aa5c045efap+6,
      0x1.c439aa1d75a25p+3,  -0x1.f3cb3cd84ac53p+3, -0x1.627d552cc8739p+7,
      0x1.c05df975f4d83p+5,  0x1.3110a511d6d27p+4,  -0x1.6f4712496f51p+6,
      0x1.f80f6e36486edp+5,  0x1.64cd65d509dcep+6,  0x1.f42ecec490c7ap+7,
      0x1.f390aac4b9456p+5,  -0x1.c9d147648e2fbp+8, -0x1.71a684a9ecb48p+4,
      0x1.3f7c04014b2d4p+3,  -0x1.5d01433a65545p+6, 0x1.9d4f3f642407p+8,
      0x1.1d79c5fc58371p+6,  0x1.e1efc2aadb7fcp+5,  0x1.50bcac3bad9fbp+2,
      0x1.524dc473d8857p+5,  0x1.bae0d2b8bb0b5p+6,  0x1.49b3047ff0a3p+6,
      -0x1.0a50d08561edap-1, 0x1.4e97fe4e18bd4p+6,  -0x1.023dbb4ef94d2p+6,
      0x1.7e86213cf102ap+8,  0x1.90283f5cc95acp+7,  0x1.286f9b3d3fe3ap+4,
      0x1.99d2673a613c4p+5,  -0x1.216f2e6d9f371p+7, -0x1.9f0513d8b5313p+7,
      0x1.427a1267a2f4p+7,   -0x1.832716f24db3fp+8, -0x1.11edde043fe9fp+5,
      0x1.03de8effa5487p+5,  0x1.2f428aa354264p+6,  0x1.8978f261712f6p+7,
      -0x1.4180c26b316fbp+8, -0x1.e1a6c5837ad06p+7, -0x1.5d0d9b76cb0cdp+6,
      0x1.1f08e7bfb68acp+6,  -0x1.774099faf7c55p+7, 0x1.697f75ed5044bp+6,
      -0x1.e48e67bf84eefp+8, -0x1.29494f9050c38p+6, -0x1.be2ab4318c388p+5,
      0x1.6919b0bf8da23p+5,  -0x1.48409d24ef787p+4, 0x1.251431b20267cp+6,
      -0x1.db450737b24aep+5, 0x1.a99c48fe6183p+6,   0x1.34181ff18252ep+4,
      0x1.8a60b75503bfcp+7,  -0x1.041f3ce2f4293p+6, -0x1.846e938a05afcp+4,
      0x1.4be3d15e3aebcp+4,  0x1.5139be467523fp-1,  0x1.1a98f63a4aedap+8,
      -0x1.7e4d68cb2584cp+5, 0x1.545ddeb7065abp+3,  -0x1.fd84bb73f06d6p+6,
      -0x1.b9bcd0b2280afp+5, -0x1.9f67402556f95p+6, 0x1.6dfccb61cd983p+7,
      -0x1.1588f93e189acp+5, -0x1.078a34511c651p+8, 0x1.763512454d631p+4,
      -0x1.30a6e81ad9569p+5, 0x1.025b8a36efc24p+6,  0x1.cb9fc3ed7cd6ep+4,
      -0x1.1c72836043e24p+8, 0x1.0f69115fbadcdp+5,  0x1.b693e396b9dfcp+6,
      -0x1.5ec07ae6d3ddbp+8, 0x1.44cd6e907d6f7p+2,  0x1.938766c7ed2e5p+2,
      0x1.9ba39d59653c6p+5,  0x1.5e1ef366ba91ap+7,  -0x1.a1e978db5e20ep+5,
      0x1.f34a186ce3aa9p+2,  -0x1.efc9f0fdebac1p+4, 0x1.70890823b8a7p+5,
      0x1.11a9e8b5040ccp+6,  0x1.2965eaf81497ep+8,  0x1.9fc7b040e6f1fp+7,
      0x1.4925f0d32c57fp+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, -2.000000, 1.000000},
     {0x1.372a5c30ead69p+5,  -0x1.c54f66ef2633dp+0, -0x1.025b0effadbaep+0,
      -0x1.dee8c48dbc8f8p+0, -0x1.274a9c087653p-2,  -0x1.4f0610ce572c4p+2,
      -0x1.05f20ee9b7c21p+1, -0x1.394fac5c99869p+1, -0x1.cd3793f8cdc62p+1,
      -0x1.e2fdfe0517d74p-1, -0x1.9c9bec48044cep-1, -0x1.1f73474ec7edcp+1,
      0x1.add3f0cf5c2b8p-2,  0x1.41adabf113f44p+0,  -0x1.74a6817e689b5p+1,
      -0x1.e4b66ff7700ccp+0, -0x1.11441ae919b35p+1, -0x1.b9307c09f7178p+1,
      -0x1.8d9e72bcca60cp+0, -0x1.da7586d27663p+0,  -0x1.606f8d0fbfd29p+1,
      -0x1.7f27367d89723p+0, -0x1.48b9df0c57f7ap+0, 0x1.65ec90259ae8p-6,
      -0x1.80521cc994089p+0, -0x1.6e58c4abecc5ap+2, -0x1.190ad01855305p+1,
      -0x1.ed5509b6c5ec7p+0, -0x1.5bb0892670c2cp+1, 0x1.58bf448d97546p+0,
      -0x1.6dc763f499a79p+0, -0x1.84e64bc6529fap+0, -0x1.f725185481032p+0,
      -0x1.aa34f18b77f7fp+0, -0x1.1c02e5a6af752p+0, -0x1.56ce3981b18a2p+0,
      -0x1.019451e0afadep+1, -0x1.544353ff53906p+0, -0x1.441d553915886p+1,
      0x1.18c6eb4cc2ee2p+0,  -0x1.89521698fdb2p-2,  -0x1.db946a2d3d856p+0,
      -0x1.97a1742607fdap+0, -0x1.9764fa324c0f5p+1, -0x1.d8a238daf70f9p+1,
      -0x1.6612f7ada36c2p-1, -0x1.49a3447092c14p+2, -0x1.249d3ac162111p+1,
      -0x1.be94528bba78dp+0, -0x1.648a50ea529fap+0, -0x1.a519fcc17cbccp-2,
      -0x1.2788b4c9f86f4p+2, -0x1.fb3f5ca1645efp+1, -0x1.5bb3bdfd9bce2p+1,
      -0x1.6cf80c9915896p+0, -0x1.c3f9a9c18aafap+1, -0x1.46496c4c3364ap+0,
      -0x1.7c3cad955c544p+2, -0x1.4e41965407addp+1, -0x1.3afb926a2ccedp+1,
      -0x1.a4491e8f13bb4p+0, -0x1.165aa323a5291p+1, -0x1.69d443466fca2p+0,
      -0x1.3ec325823f0d1p+1, -0x1.24fb47cba572dp+0, -0x1.da10d0a17762fp+0,
      -0x1.a156ca4199fa8p-2, -0x1.449a6641992f4p+1, -0x1.1a4306417b61ep+1,
      -0x1.d6f9af9d521d4p+0, -0x1.005acabb13eb1p+1, 0x1.248cfb3e46e2p-2,
      -0x1.32b04974261e9p+1, -0x1.ebf9e7906000cp+0, -0x1.8561a50d7d9e1p+1,
      -0x1.3a68504c54fe2p+1, -0x1.6cef98f913441p+1, -0x1.0ba991e84e244p-1,
      -0x1.25151d0b639c6p+1, -0x1.096c5dabcc41cp+2, -0x1.d17ac51b9235ap+0,
      -0x1.289aa9c7a4899p+1, -0x1.7bddbed97ccdp+0,  -0x1.c662f08764269p+0,
      -0x1.1448c93bfb5e4p+2, -0x1.bb94eadd293bp+0,  -0x1.1e3ecf971a748p+0,
      -0x1.36ba6e63f65f4p+2, -0x1.f7884a8a4512p+0,  -0x1.f4f9f106cebdbp+0,
      -0x1.97289e9c50184p+0, -0x1.2ca1c3bcd2c42p-1, -0x1.375030f144be7p+1,
      -0x1.f1de028819daep+0, -0x1.213ba89579b16p+1, -0x1.a25abeb78d8b8p+0,
      -0x1.73ea3aa54a5d2p+0, 0x1.9f919a571c34p-2,   -0x1.486508eba0a5p-2,
      -0x1.5836262e99e24p-1}},
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, -2.000000, 123.200000},
     {0x1.3ac592557ba1cp+12, 0x1.a3e9676826a44p+4,  0x1.e043e98704c51p+6,
      0x1.bd98f9dfa82a9p+3,  0x1.a1bed8dac3874p+7,  -0x1.908546c067cecp+8,
      -0x1.ee3ec9d69de53p+2, -0x1.c94be2629e25dp+5, -0x1.8f0afcd88c1d7p+7,
      0x1.005c2a8d8c86dp+7,  0x1.223b5cb08abbp+7,   -0x1.022ad84503737p+5,
      0x1.281d195f5916dp+8,  0x1.8f352df26b33fp+8,  -0x1.c91aa5c045efap+6,
      0x1.6439aa1d75a25p+3,  -0x1.29e59e6c2562ap+4, -0x1.687d552cc8739p+7,
      0x1.a85df975f4d83p+5,  0x1.0110a511d6d27p+4,  -0x1.7b4712496f51p+6,
      0x1.e00f6e36486edp+5,  0x1.58cd65d509dcep+6,  0x1.ee2ecec490c7ap+7,
      0x1.db90aac4b9456p+5,  -0x1.ccd147648e2fbp+8, -0x1.a1a684a9ecb48p+4,
      0x1.bef80802965a9p+2,  -0x1.6901433a65545p+6, 0x1.9a4f3f642407p+8,
      0x1.1179c5fc58371p+6,  0x1.c9efc2aadb7fcp+5,  0x1.217958775b3f6p+1,
      0x1.3a4dc473d8857p+5,  0x1.aee0d2b8bb0b5p+6,  0x1.3db3047ff0a3p+6,
      -0x1.c2943421587b7p+1, 0x1.4297fe4e18bd4p+6,  -0x1.0e3dbb4ef94d2p+6,
      0x1.7b86213cf102ap+8,  0x1.8a283f5cc95acp+7,  0x1.f0df367a7fc73p+3,
      0x1.81d2673a613c4p+5,  -0x1.276f2e6d9f371p+7, -0x1.a50513d8b5313p+7,
      0x1.3c7a1267a2f4p+7,   -0x1.862716f24db3fp+8, -0x1.29edde043fe9fp+5,
      0x1.d7bd1dff4a90dp+4,  0x1.23428aa354264p+6,  0x1.8378f261712f6p+7,
      -0x1.4480c26b316fbp+8, -0x1.e7a6c5837ad06p+7, -0x1.690d9b76cb0cdp+6,
      0x1.1308e7bfb68acp+6,  -0x1.7d4099faf7c55p+7, 0x1.5d7f75ed5044bp+6,
      -0x1.e78e67bf84eefp+8, -0x1.35494f9050c38p+6, -0x1.d62ab4318c388p+5,
      0x1.5119b0bf8da23p+5,  -0x1.78409d24ef787p+4, 0x1.191431b20267cp+6,
      -0x1.f3450737b24aep+5, 0x1.9d9c48fe6183p+6,   0x1.04181ff18252ep+4,
      0x1.8460b75503bfcp+7,  -0x1.101f3ce2f4293p+6, -0x1.b46e938a05afcp+4,
      0x1.1be3d15e3aebcp+4,  -0x1.2bb1906e62b7p+1,  0x1.1798f63a4aedap+8,
      -0x1.964d68cb2584cp+5, 0x1.e8bbbd6e0cb56p+2,  -0x1.04c25db9f836bp+7,
      -0x1.d1bcd0b2280afp+5, -0x1.ab67402556f95p+6, 0x1.67fccb61cd983p+7,
      -0x1.2d88f93e189acp+5, -0x1.0a8a34511c651p+8, 0x1.463512454d631p+4,
      -0x1.48a6e81ad9569p+5, 0x1.ecb7146ddf848p+5,  0x1.9b9fc3ed7cd6ep+4,
      -0x1.1f72836043e24p+8, 0x1.eed222bf75b99p+4,  0x1.aa93e396b9dfcp+6,
      -0x1.61c07ae6d3ddbp+8, 0x1.099add20fadeep+1,  0x1.a70ecd8fda5c9p+1,
      0x1.83a39d59653c6p+5,  0x1.581ef366ba91ap+7,  -0x1.b9e978db5e20ep+5,
      0x1.334a186ce3aa9p+2,  -0x1.0fe4f87ef5d61p+5, 0x1.58890823b8a7p+5,
      0x1.05a9e8b5040ccp+6,  0x1.2665eaf81497ep+8,  0x1.99c7b040e6f1fp+7,
      0x1.4325f0d32c57fp+7} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 123.200000, 1.000000},
     {0x1.4830fd72a11c1p+7, 0x1.edb78f311034p+6,  0x1.f0c36090ce15ep+6,
      0x1.ed5129ba95da9p+6, 0x1.f3a58230c4568p+6, 0x1.dfdc6bbfe75a1p+6,
      0x1.ec9d3c557f0ecp+6, 0x1.eb024f69e800ap+6, 0x1.e663102d065eap+6,
      0x1.f106d0d0c29d2p+6, 0x1.f19394f43cc43p+6, 0x1.ebd13292568d6p+6,
      0x1.f67aa0bd9c29p+6,  0x1.f9d3837c911cap+6, 0x1.e92798c0d987fp+6,
      0x1.ed39f30cef0cap+6, 0x1.ec42abf583ff3p+6, 0x1.e70348ec7d141p+6,
      0x1.ee965301d9a35p+6, 0x1.ed62f6b182f34p+6, 0x1.e9c950644ece4p+6,
      0x1.eed02ff2d6a7p+6,  0x1.efa9e5509b6cfp+6, 0x1.f4e32b95cf268p+6,
      0x1.eecb8459a67cbp+6, 0x1.dde740820e007p+6, 0x1.ec04764c0a235p+6,
      0x1.ed1778a5f1b52p+6, 0x1.e9ef48839946cp+6, 0x1.fa2fc9df032a2p+6,
      0x1.ef15af3cfa663p+6, 0x1.eeb9339db3825p+6, 0x1.ecf0386b7ac8cp+6,
      0x1.ee23f9069eecfp+6, 0x1.f05cc136320fp+6,  0x1.ef7193e6c606ap+6,
      0x1.ecc02a3dc74f6p+6, 0x1.ef7bbf7ccf7e9p+6, 0x1.eaabe22304209p+6,
      0x1.f92fe879ffd89p+6, 0x1.f3437ab633cf2p+6, 0x1.ed5e7b2417d6cp+6,
      0x1.ee6e46fc34acep+6, 0x1.e811a4fb3a6c5p+6, 0x1.e607bb05f5145p+6,
      0x1.f200a6dd7185fp+6, 0x1.e0329885c3a0cp+6, 0x1.eba7e2f6c1bc4p+6,
      0x1.edd27b829de2fp+6, 0x1.ef3aa38923825p+6, 0x1.f327b2d00b501p+6,
      0x1.e25441802d45ep+6, 0x1.e4f2d1e7c1a9ep+6, 0x1.e9ef2edcdfee6p+6,
      0x1.ef18ec9a6876bp+6, 0x1.e6acff7ec0775p+6, 0x1.efb3a71b9bff4p+6,
      0x1.dd0901f377079p+6, 0x1.ea5ac01a2c8f6p+6, 0x1.eaf4f0397b666p+6,
      0x1.ee3ba852907dep+6, 0x1.ec19f7b3afa38p+6, 0x1.ef257bbfb30dap+6,
      0x1.ead6b3a0bad46p+6, 0x1.f038dfad9e37p+6,  0x1.ed64898a46ef4p+6,
      0x1.f32b76028b32dp+6, 0x1.eaa7f99ac0035p+6, 0x1.ebfab49ac0f1cp+6,
      0x1.ed70e60e57846p+6, 0x1.ecc9f676f42d7p+6, 0x1.f5f159c80b13bp+6,
      0x1.eb374a812b9bep+6, 0x1.ed1ce52e8b4cdp+6, 0x1.e8a1bfa460dfep+6,
      0x1.eaf98a4a6a24ep+6, 0x1.e96550050432bp+6, 0x1.f2b579a8fc308p+6,
      0x1.eba423e471affp+6, 0x1.e43606f21008bp+6, 0x1.ed86e1b85e84p+6,
      0x1.eb87f77e8fa88p+6, 0x1.eedd55d166d9ap+6, 0x1.edb3410aaf3c3p+6,
      0x1.e38840390d16fp+6, 0x1.edde79215827ep+6, 0x1.f053d18e7063p+6,
      0x1.e16125e68d66ep+6, 0x1.eceeaba2a3b89p+6, 0x1.ecf8e508b191ep+6,
      0x1.ee702a525b8c7p+6, 0x1.f273894553274p+6, 0x1.eb124b4542a6ep+6,
      0x1.ed0554c2ac656p+6, 0x1.ebc2ef8820ff4p+6, 0x1.ee4361d1ee96ap+6,
      0x1.eefd23e237a36p+6, 0x1.f66c5e6723e9p+6,  0x1.f38467c3e12c3p+6,
      0x1.f21c60806f991p+6} },
    {{VSL_BRNG_MCG59, VSL_RNG_METHOD_LAPLACE_ICDF, 0, 123.200000, 123.200000},
     {0x1.4298c588aed4fp+12, 0x1.2ee393536b3afp+7,  0x1.ea885b29e8c8fp+7,
      0x1.163ff60460e91p+7,  0x1.4e129fa094f6dp+8,  -0x1.1352138d349b8p+8,
      0x1.d5e8e02f62ee8p+6,  0x1.1026db9b7db9fp+6,  -0x1.29492ce44b6e2p+6,
      0x1.fac290f3f2ed4p+7,  0x1.0e50e18b7890bp+8,  0x1.73b760aa4b132p+6,
      0x1.a5504c928c4a1p+8,  0x1.06343092cf339p+9,  0x1.5d91386436e98p+3,
      0x1.10aa01083dc09p+7,  0x1.aa536531c3743p+6,  -0x1.b85bbb198834cp+5,
      0x1.647de4c3e39c7p+7,  0x1.1a887b08a140bp+7,  0x1.e616ea0d75ef3p+4,
      0x1.726a41f3f8822p+7,  0x1.a6cd1950eb54dp+7,  0x1.744a9a957b97p+8,
      0x1.714a911794b7cp+7,  -0x1.4f9e14315afc8p+8, 0x1.8c632ba2519fbp+6,
      0x1.085e26a67b194p+7,  0x1.17971324cef1p+5,   0x1.0bc1394bab9d2p+9,
      0x1.832349649281fp+7,  0x1.6ce257111d465p+7,  0x1.fdd8979087a6dp+6,
      0x1.48f9d7835c87cp+7,  0x1.d1d6cfc2c3ec1p+7,  0x1.993fe8a65eb7ep+7,
      0x1.e6b82b2bc208fp+6,  0x1.9bb2658d72c5p+7,   0x1.cd1e22fba6ff6p+5,
      0x1.f8b954702435dp+8,  0x1.424752e197e09p+8,  0x1.197459ce0e62ep+7,
      0x1.5adb0034feb57p+7,  -0x1.68464039c6857p+4, -0x1.553d5ae49d958p+6,
      0x1.1b703c6704ad3p+8,  -0x1.08f3e3bf1a80cp+8, 0x1.5fd5ddcaacd7ep+6,
      0x1.355e0a264fb88p+7,  0x1.8c07abb810799p+7,  0x1.3eefac63ebcaep+8,
      -0x1.8e9b1e6ffc79p+7,  -0x1.da80be3a28d3fp+6, 0x1.177e62ac03801p+5,
      0x1.83eada4641abdp+7,  -0x1.05b4672922bdcp+6, 0x1.a926215d0e88cp+7,
      -0x1.6a5b348c51bbcp+8, 0x1.7f06fa78f8129p+5,  0x1.09b772b406b09p+6,
      0x1.4eacd29649cefp+7,  0x1.96bca58390eebp+6,  0x1.86f07f3f679a5p+7,
      0x1.f6549261e74ecp+5,  0x1.c9348ae59727ep+7,  0x1.1ae96a6496b0cp+7,
      0x1.3f638eddb5131p+8,  0x1.c95b1fd3b1474p+5,  0x1.87b127ea4b60ep+6,
      0x1.1de2e0922dc3ep+7,  0x1.eb6f404959b71p+6,  0x1.94cc296d7e20dp+8,
      0x1.29a618673a0a7p+6,  0x1.09ac4451d6cc1p+7,  -0x1.4b7eea723a08bp+2,
      0x1.0bee6473b8c76p+6,  0x1.2596329dd74ep+4,   0x1.313198e419ff5p+8,
      0x1.5e08502dc07f7p+6,  -0x1.1aae023bd263bp+7, 0x1.232d08af1012dp+7,
      0x1.507958bf60219p+6,  0x1.75942b81de478p+7,  0x1.2dda5ee416014p+7,
      -0x1.447ea05a215e1p+7, 0x1.3840aabe551dap+7,  0x1.cfb05831c3564p+7,
      -0x1.c91a8f674155p+7,  0x1.fd19a3b5d4a3cp+6,  0x1.0102a19ca5cfep+7,
      0x1.5b4f4dbcbfb58p+7,  0x1.2942ace6907cp+8,   0x1.17d8105f1dbc6p+6,
      0x1.0400b729cd83cp+7,  0x1.6cda508d51e1dp+6,  0x1.5088a86f54902p+7,
      0x1.7d3b5ac0e86ccp+7,  0x1.a3991e2b47cb1p+8,  0x1.4a170b53a6ac3p+8,
      0x1.1ec62b9cc95f3p+8} }
};

template <typename T> auto reference();

template <> auto reference<float>() { return float_reference; }

template <> auto reference<double>() { return double_reference; }

} // namespace LaplaceGolden
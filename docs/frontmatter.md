# Open Random Number Generation (OpenRNG) Reference Guide

Copyright © 2024 Arm Limited (or its affiliates). All rights reserved.

# About this book

The following documentation describes OpenRNG, the random number generator (RNG)
interface provided in \armplref.

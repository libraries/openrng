## Description

Summarize the purpose of this Merge Request and include links to any relevant issues.


## Checklist

Please ensure:

* [You are familiar with the contribution guidelines](https://gitlab.arm.com/libraries/openrng/-/blob/main/CONTRIBUTING.md)
* [All new functionality has sufficient test coverage](https://gitlab.arm.com/libraries/openrng/-/blob/main/CONTRIBUTING.md#unit-tests)
* [All unit tests are passing](https://gitlab.arm.com/libraries/openrng/-/blob/main/CONTRIBUTING.md#unit-tests)
* [Any C++ code has been formatted with `clang-format`](https://gitlab.arm.com/libraries/openrng/-/blob/main/CONTRIBUTING.md#coding-style)
* [The changelog is up to date](https://gitlab.arm.com/libraries/openrng/-/blob/main/CHANGELOG.md#unreleased)
* [Implementation statuses are up to date](https://gitlab.arm.com/libraries/openrng/-/blob/main/IMPLEMENTATION_STATUS.md)
* [Your contribution meets OpenRNG's license terms](https://gitlab.arm.com/libraries/openrng/-/blob/main/CONTRIBUTING.md#legal-requirements)
